import pickle
import re

from flask import Flask, render_template, request, jsonify, redirect, session

app = Flask(__name__, static_url_path='/static', static_folder='static', template_folder='templates')


class Lodka:
    def __init__(self, jezdci):
        self.jezdci = jezdci


class Jezdec:
    def __init__(self, jmeno, prijmeni, trida, nick, heslo, je_plavec, kanoe_kamarad):
        self.jmeno = jmeno
        self.prijmeni = prijmeni
        self.trida = trida
        self.nick = nick
        self.heslo = heslo
        self.je_plavec = je_plavec
        self.kanoe_kamarad = kanoe_kamarad
        self.pozvanky = []


jezdci = []
lodky = []
tridy = ['A3', 'E3', 'C3a', 'C3b', 'C3c', 'A4', 'E4', 'C4a', 'C4b', 'C4c']

try:
    with open('data_jezdci.pkl', 'rb') as f_jezdci:
        jezdci = pickle.load(f_jezdci)
except FileNotFoundError:
    jezdci = []

try:
    with open('data_lodky.pkl', 'rb') as f_lodky:
        lodky = pickle.load(f_lodky)
except FileNotFoundError:
    lodky = []


def save_jezdci():
    with open('data_jezdci.pkl', 'wb') as f:
        pickle.dump(jezdci, f)


def save_lodky():
    with open('data_lodky.pkl', 'wb') as f:
        pickle.dump(lodky, f)


@app.teardown_appcontext
def on_teardown_appcontext(exception=None):
    save_jezdci()
    save_lodky()


@app.teardown_appcontext
def on_teardown_appcontext(exception=None):
    save_jezdci()
    save_lodky()


@app.route('/', methods=['GET'])
def index():
    if 'nick' in session:
        return redirect('/jezdec'), 302
    return render_template('index.html'), 200


@app.route('/registrace', methods=['GET', 'POST'])
def registrace():
    if request.method == 'GET':
        return render_template('registrace.html', tridy=tridy), 200

    if request.method == 'POST':
        nick = request.form.get("nick")
        je_plavec = request.form.get("je_plavec")
        heslo = request.form.get("heslo")
        kanoe_kamarad = request.form.get("kanoe_kamarad")
        jmeno = request.form.get("jmeno")
        prijmeni = request.form.get("prijmeni")
        trida = request.form.get("trida")

        if not re.match("^[a-z0-9]{2,20}$", nick):
            return "Přezdívka musí obsahovat 2 až 20 znaků, a může obsahovat pouze malá písmena a čísla.", 400

        if any(jezdec.nick == nick for jezdec in jezdci):
            return "Tento nickname již existuje.", 400

        if not re.match("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,20}$", heslo):
            return "Heslo musí obsahovat alespoň jedno malé písmeno, jedno velké písmeno a jednu číslici. Délka hesla by měla být mezi 8 a 20 znaky.", 400

        if not re.match("^[A-Ž][a-ž]{1,20}$", jmeno):
            return "Jméno musí obsahovat pouze 2 až 20 znaků a smí obsahovat pouze písmenaa začáteční písmeno musí být velké.", 400

        if not re.match("^[A-Ž][a-ž]{1,20}$", prijmeni):
            return "Příjmení musí obsahovat pouze 2 až 20 znaků a smí obsahovat pouze písmena a začáteční písmeno musí být velké.", 400

        if trida not in tridy:
            return "Třída neexistuje", 400

        if je_plavec != 'on':
            return "Musíte označit, zda jste plavec.", 400

        if kanoe_kamarad and not re.match("^[a-zA-Z0-9]{2,20}$", kanoe_kamarad):
            return "Přezdívka pro spolujezdce může obsahovat pouze 2 až 20 znaků a smí obsahovat pouze písmena a čísla.", 400

        if kanoe_kamarad:
            if not any(jezdec.nick == kanoe_kamarad for jezdec in jezdci):
                return "Spolujezdec neexistuje.", 400

            else:
                kamarad = next((jezdec for jezdec in jezdci if jezdec.nick == kanoe_kamarad), None)
                kamarad.pozvanky.append(jmeno + ' ' + prijmeni)
                save_jezdci()

        jezdci.append(Jezdec(jmeno, prijmeni, trida, nick, heslo, je_plavec, kanoe_kamarad))
        return redirect('/'), 302


@app.route('/api/check-nickname', methods=['GET'])
def check_nickname():
    requested_nick = request.args.get('nick')
    if any(jezdec.nick == requested_nick for jezdec in jezdci):
        return jsonify({'exists': True})
    else:
        return jsonify({'exists': False})


@app.route('/api/check-login', methods=['GET'])
def check_login():
    requested_nick = request.args.get('nick')
    requested_heslo = request.args.get('heslo')

    if any(jezdec.nick == requested_nick and jezdec.heslo == requested_heslo for jezdec in jezdci):
        return jsonify({'exists': True})
    else:
        return jsonify({'exists': False})


@app.route('/jezdec', methods=['GET'])
def jezdec():
    if 'nick' not in session:
        return redirect('/prihlaseni'), 302

    vybrana_jmena = request.args.get('jmeno', '')
    vybrana_prijmeni = request.args.get('prijmeni', '')
    vybrana_trida = request.args.get('trida', '')

    session['filtr_jmeno'] = vybrana_jmena
    session['filtr_prijmeni'] = vybrana_prijmeni
    session['filtr_trida'] = vybrana_trida

    filtrovani_jezdcu = filtrovat_jezdce(vybrana_jmena, vybrana_prijmeni, vybrana_trida)

    return render_template('jezdec.html', jezdci=filtrovani_jezdcu, tridy=tridy), 200


def match(jezdec, jezdci):
    index_jezdec = 0
    index_jezdci = 0

    while index_jezdec < len(jezdec) and index_jezdci < len(jezdci):
        if jezdec[index_jezdec].lower() == jezdci[index_jezdci].lower():
            index_jezdec += 1
        index_jezdci += 1

    return index_jezdec == len(jezdec)


def filtrovat_jezdce(jmena, prijmeni, trida):
    filtrovani_jezdcu = jezdci

    if jmena:
        filtrovani_jezdcu = [jezdec for jezdec in filtrovani_jezdcu if match(jmena, jezdec.jmeno)]

    if prijmeni:
        filtrovani_jezdcu = [jezdec for jezdec in filtrovani_jezdcu if match(prijmeni, jezdec.prijmeni)]

    if trida and trida != '---':
        filtrovani_jezdcu = [jezdec for jezdec in filtrovani_jezdcu if jezdec.trida == trida]

    return filtrovani_jezdcu


@app.route('/prihlaseni', methods=['GET', 'POST'])
def prihlaseni():
    if request.method == 'GET':
        return render_template('prihlaseni.html'), 200

    if request.method == 'POST':
        requested_nick = request.form.get("nick")
        requested_heslo = request.form.get("heslo")

        if any(jezdec.nick == requested_nick and jezdec.heslo == requested_heslo for jezdec in jezdci):
            session['nick'] = requested_nick
            return redirect('jezdec'), 302

        return "Neplatné uživatelské jméno nebo heslo.", 400


@app.route('/odhlaseni', methods=['GET'])
def odhlaseni():
    session.pop('nick', None)
    session.pop('filtr_jmeno', None)
    session.pop('filtr_prijmeni', None)
    session.pop('filtr_trida', None)
    return redirect('/'), 302


@app.route('/potvrdit-pozvanku/<jezdec_nick>/<pozvanka_id>', methods=['GET'])
def potvrdit_pozvanku(jezdec_nick, pozvanka_id):
    if 'nick' not in session:
        return redirect('/prihlaseni'), 302

    jezdec = next((jezdec for jezdec in jezdci if jezdec.nick == jezdec_nick), None)

    if jezdec:
        pozvanka = next((pozvanka for pozvanka in jezdec.pozvanky if str(pozvanka.id) == pozvanka_id), None)

        if pozvanka:
            jezdec.pozvanky.remove(pozvanka)

            jezdec.kanoe_kamarad = pozvanka.od_koho

            spolujezdec = next((jezdec for jezdec in jezdci if jezdec.nick == pozvanka.od_koho), None)
            spolujezdec.kanoe_kamarad = jezdec

            save_jezdci()

            lodka = next((lodka for lodka in lodky if not lodka.jezdci), None)
            if lodka:
                lodka.jezdci.append(jezdec)
                save_lodky()

            return redirect('/jezdec'), 302

    return "Pozvánka neexistuje.", 400


@app.route('/odmitnout-pozvanku/<jezdec_nick>/<pozvanka_id>', methods=['GET'])
def odmitnout_pozvanku(jezdec_nick, pozvanka_id):
    if 'nick' not in session:
        return redirect('/prihlaseni'), 302

    spolujezdec = next((jezdec for jezdec in jezdci if jezdec.nick == jezdec_nick), None)

    if jezdec:
        pozvanka = next((pozvanka for pozvanka in jezdec.pozvanky if str(pozvanka.id) == pozvanka_id), None)

        if pozvanka:
            jezdec.pozvanky.remove(pozvanka)
            save_jezdci()

            return redirect('/jezdec'), 302

    return "Pozvánka neexistuje.", 400


if __name__ == '__main__':
    app.run(host='localhost', port=8090)
