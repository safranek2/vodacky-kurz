function validateRegistration(form) {
    let jmeno = form.querySelector("#fjmeno").value;
    let prijmeni = form.querySelector("#fprijmeni").value;
    let nick = form.querySelector("#fnick").value;
    let heslo = form.querySelector("#fheslo").value;
    let je_plavec = document.getElementById("rplavec").checked;
    let kanoeKamarad = form.querySelector("#fspolujezdec").value;
    let trida = form.querySelector("#strida").value;

    if (nick.length < 2 || nick.length > 20 || !/^[a-z0-9]+$/.test(nick)) {
        alert("Přezdívka musí obsahovat 2 až 20 znaků a smí obsahovat pouze malá písmena a čísla.");
        return false;
    }

    let xhr = new XMLHttpRequest();
    xhr.open("GET", "/api/check-nickname?nick=" + nick, false);
    xhr.send();

    if (xhr.status === 200) {
        let response = JSON.parse(xhr.responseText);
        if (response.exists) {
            alert("Tento nick již existuje.");
            return false;
        }
    }
    if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,20}$/.test(heslo)) {
        alert("Heslo musí obsahovat alespoň jedno malé písmeno, jedno velké písmeno a jednu číslici. Délka hesla by měla být mezi 8 a 20 znaky.");
        return false;
    } else if (!/^[A-Ž][a-ž]{1,20}$/.test(jmeno)) {
        alert("Jméno musí obsahovat pouze 2 až 20 znaků, začáteční písmeno musí být velké.");
        return false;
    } else if (!/^[A-Ž][a-ž]{1,20}$/.test(prijmeni)) {
        alert("Příjmení musí obsahovat pouze 2 až 20 znaků, začáteční písmeno musí být velké.");
        return false;
    } else if (trida === '---') {
        alert("Vyberte prosím třídu.");
        return false;
    } else if (!je_plavec) {
        alert("Musíte označit, zda jste plavec.");
        return false;
    } else if (kanoeKamarad && (kanoeKamarad.length < 2 || kanoeKamarad.length > 20 || !/^[a-z0-9]+$/.test(kanoeKamarad))) {
        alert("Přezdívka pro spolujezdce může obsahovat pouze 2 až 20 znaků a smí obsahovat pouze malá písmena a čísla.");
        return false;
    }

    let xhr2 = new XMLHttpRequest();
    xhr2.open("GET", "/api/check-nickname?nick=" + kanoeKamarad, false);
    xhr2.send();

    if (kanoeKamarad) {
        if (xhr2.status === 200) {
            let response = JSON.parse(xhr2.responseText);
            if (!response.exists) {
                alert("Tento spolujezdec neexistuje.");
                return false;
            }
        }
    }

    confirm('Registrace proběhla úspěšně.')

    return true;
}

function validateLogin(form) {
    console.log("Validating login...");
    let nick = form.querySelector("#fnick").value;
    let heslo = form.querySelector("#fheslo").value;

    let xhr3 = new XMLHttpRequest();
    xhr3.open("GET", "/api/check-login?nick=" + nick + "&heslo=" + heslo, false);
    xhr3.send();

    if (xhr3.status === 200) {
        let response = JSON.parse(xhr3.responseText);
        console.log("Server response:", response);
        if (!response.exists) {
            alert("Neplatná přezdívka nebo heslo.");
            return false;
        }
    }

    confirm('Přihlášení proběhla úspěšně.')

    return true;
}
